<?php 
include('header.php');
if(isset($_SESSION['role'])){
	if($_SESSION['role'] ==0){
		header('location:contributeur.php');
	}
}
else{
	header('location:contributeur.php');
}
?>

<div id="map" style="margin-left:12%;width: 75%; height: 50%;"></div>
<?php 
	$marqueur = array();

	$request = $dbh->query('SELECT * FROM marqueur');
	while($donnees = $request ->fetch()){
		$position = array();
		array_push($position,intval($donnees['IDmarqueur']));
		array_push($position,doubleval($donnees['x']));
		array_push($position,doubleval($donnees['y']));
		array_push($marqueur,$position);
	}
?>

<?php 
include('footer.php');
?>

<script>
		var markers = []
		var map = L.map('map', {
		crs: L.CRS.Simple,
		minZoom: -1,maxZoom:10
		});
		var bounds = [[-26.5,-25], [1021.5,1023]];
		var image = L.imageOverlay('images/reconstitue.png', bounds).addTo(map);
		map.fitBounds(bounds);
		map.setView([50.505, -0.09], 1);
		function createMarker(coords,ids) {
		  var id
		  id =ids
		  var popupContent =
		    '<p>Some Infomation</p></br>' +
		    '<p>test</p></br>';
		  myMarker = L.marker(coords, {
		    draggable: false
		  });
		  myMarker._id = id
		  var myPopup = myMarker.bindPopup(popupContent, {
		    closeButton: false
		  });
		  map.addLayer(myMarker)
		  markers.push(myMarker)
		}

		var coordinates = <?php echo json_encode($marqueur);?>;
		for (i = 0;i<coordinates.length;i++){
		var coord = []
		coord.push(coordinates[i][1]);
		coord.push(coordinates[i][2]);
		console.log(coordinates[i]);
		createMarker(coord,coordinates[i][0]);
	    	}
</script>