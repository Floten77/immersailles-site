
<?php
	include('header.php');
	include('fonctions.php');
?>
<?php
	if(!isset($_SESSION['login'])){
		header('location:index.php');
	}
	//Verifie si l'utilisateur appuie sur le bouton supprimer
	if(isset($_POST['delete'])){
		$req = 'DELETE FROM marqueur WHERE IDmarqueur = ?';
		$request = $dbh->prepare($req);
		//Supprime le marqueur
		$request->execute(array($_POST['idmarqueur']));
	}
?>
<?php
	//Verifie si l'utilisateur a bien selectionné un niveau avant d'afficher la carte
	if(isset($_POST['selectlevel']) || isset($_POST['delete'])){
		if($_POST['selectedlevel'] !=""){
			 $sep = explode("-",$_POST['selectedlevel']);
			 //On recupère la carte correspondant au niveau choisit
			 $url = $dbh->prepare('SELECT Cartes FROM niveaux,associer WHERE associer.IdLieu = niveaux.IdLieu AND IDannées = ? AND associer.IdLieu = ?  ');
		     $url->execute(array($sep[0],$sep[1]));
		    $res = $url->fetch();
		    //$tabl va nous permettre de stocker la carte et l'envoyer dans le JS
		    $tabl = array();
		    if($res['Cartes'] !=""){
		        array_push($tabl,$res['Cartes']);
		    }
		    else{
		        array_push($tabl,"");
		    }
		}
	}
	//Dans le cas où le niveau est pas selectionné
	else{
		$tabl = array();
		array_push($tabl,"");
	}
	//Tableau qui stocke les marqueurs
	$marqueur = array();
	$request = $dbh->prepare('SELECT * FROM marqueur WHERE IdLieu = ?');
	if(isset($_POST['selectlevel']) || isset($_POST['delete'])){
		$request->execute(array($sep[1]));
		while($donnees = $request ->fetch()){
		$position = array();
		array_push($position,intval($donnees['IDmarqueur']));
		array_push($position,doubleval($donnees['x']));
		array_push($position,doubleval($donnees['y']));
		array_push($marqueur,$position);
	}
	}
	if(isset($_POST['selectlevel']) ||isset($_POST['delete'])){
	//$database stocke les informations des marqueurs
	$database = array();
    $req = "SELECT IDmarqueur,marqueur.IDObject,NomObjet FROM marqueur,objethistoriques WHERE marqueur.IDObject = objethistoriques.IDObject";
    $answer  = $dbh -> prepare($req);
    $answer ->execute();
    while ($data = $answer ->fetch()){
        $datatable = array();
        //Ajout des informations selon le type de IDObject 
        if(isPersonnage($dbh,$data['NomObjet'])){
           $info = getObjectDecoded($dbh,$data['NomObjet']);
            array_push($datatable,givePhotoObject($dbh, 250, $data['NomObjet'],$info));
            array_push($datatable,getTypeObject($dbh, $data['NomObjet']));
            array_push($datatable ,giveValueName($dbh,$data['NomObjet'],$info));
            array_push($datatable ,giveValueDateOfCreation($dbh,$data['NomObjet'],$info));
            array_push($datatable ,giveValueDateOfDeath($dbh,$data['NomObjet'],$info));
            array_push($datatable ,giveDescription($dbh,$data['NomObjet'],$info));
        
        }
        if(isOeuvre($dbh,$data['NomObjet'])){
             $info = getObjectDecoded($dbh,$data['NomObjet']);
            array_push($datatable,givePhotoObject($dbh, 250, $data['NomObjet'],$info));
           array_push($datatable,giveValueName($dbh,$data['NomObjet'],$info));
           array_push($datatable,giveValueDateOfCreation($dbh, $data['NomObjet'],$info));
           array_push($datatable,giveDescription($dbh, $data['NomObjet'],$info));
        }
        if(isMobilier($dbh,$data['NomObjet'])){
        	$info = getObjectDecoded($dbh,$data['nomObjet']);
        	array_push($datatable,givePhotoObject($dbh,250,$data['NomObjet'],$info));
        	array_push($datatable,giveValueName($dbh,$data['NomObjet'],$info));
           array_push($datatable,giveValueDateOfCreation($dbh, $data['NomObjet'],$info));
           array_push($datatable,giveDescription($dbh, $data['NomObjet'],$info));
        }
    $database[$data['IDmarqueur']] = $datatable;    
    }
	}

?>
<center><p class="h2">Selectionner un niveau</p></center>
<?php
	//Affichage du niveau selectionné
	if(isset($_POST["selectlevel"]) || isset($_POST['delete'])){
		if($_POST['selectedlevel'] != ""){
			if($sep[1]==101)
			echo "<p style='margin-left:42%'>Niveau selectionnée : Année:".$sep[0]."-RDC</p>";
			if($sep[1]==102)
				echo "<p style='margin-left:42%'>Niveau selectionnée : Année:".$sep[0]."-1ere étage</p>";
			if($sep[1]==103)
				echo "<p style='margin-left:42%'>Niveau selectionnée : Année:".$sep[0]."-2eme étage</p>";
		}
	}
?>
<form style="margin-left:45%" method="POST">
	<select name="selectedlevel" >
		<?php
		//Afficher tous les niveaux associer dans le back-office
		$req = 'SELECT IDannées,associer.IdLieu,nomlieu FROM associer,niveaux WHERE associer.IdLieu = niveaux.IdLieu';
		$request = $dbh->query($req);
		while($donnees = $request->fetch()){
			echo "<option value =".$donnees['IDannées']."-".$donnees['IdLieu'].">".$donnees['IDannées']."-".$donnees['nomlieu']."</option>";
		}
		?>
	</select>
	<input type="submit" value="Valider" name="selectlevel">
</form>
<?php
	//Si le niveau est selectionné on affiche la carte
	if(isset($_POST['selectlevel']) || isset($_POST['delete'])){
?>
 <div class="d-flex flex-row" style="height:50%">
<div id="map" style="width:75%;margin-left:12%; height: 100%;"></div>
<section class="w-25 bg-dark" style="height:100% ;display:none; overflow-y: scroll;" id ="test">	
       <div class="h-75 d-flex flex-column " id="database">
        </div>
    </section>
<?php } ?>
	</div>
<?php
	if(isset($_POST['selectlevel']) || isset($_POST['delete'])){
?>
<form style="margin-left:35%" method="POST" action="">
<label>IDMarqueur:</label><input id ="id" name="idmarqueur" type ="text" readonly/> 
<input id ="longitude" type ="hidden" readonly/> 
<input id ="latitude" type ="hidden" readonly/>
<input type="submit" name="delete" value="Supprimer le marqueur"> 
<?php if (isset($_POST['selectedlevel'])){?>


<input type="hidden" name="selectedlevel" value ="<?php echo $_POST['selectedlevel']; ?>"/>
</form>

<?php
	}
	}
	if(isset($_POST['delete'])){
		echo '<p style="margin-left:38%">Le marqueur selectionné a bien été supprimer</p>';
	}
?>


<script async>
		var databases = <?php echo json_encode($database); ?>; // Stocke les informations des objets historiques qui sont affectés à un ID marqueur
        //Generation de la carte
		var map = L.map('map', {
		crs: L.CRS.Simple,
		minZoom: -1,maxZoom:10
		});
		var bounds = [[-26.5,-25], [1021.5,1023]];
        if(<?php echo json_encode($tabl[0]); ?> !=""){
             var url = "reconnaissance/affectation/"+<?php echo json_encode($tabl[0]); ?>;
        }
        else{
            var url="images/reconstitue.png";
        }
       
        console.log(url);

        var image = L.imageOverlay(url, bounds).addTo(map);
        map.fitBounds(bounds);
        map.setView([328.505, 486], 1);

		var markers = []
		function createMarker(coords,ids) {
				  var id
				  id =ids
				  var popupContent =
				    '<p>Some Infomation</p></br>' +
				    '<p>test</p></br>'+'<button onclick="clearMarker(' + id + ')">Select marker</button>';
				  myMarker = L.marker(coords, {
				    draggable: false
				  });
				  myMarker._id = id
				  var myPopup = myMarker.bindPopup(popupContent, {
				    closeButton: false
				  });
				  map.addLayer(myMarker)
				  markers.push(myMarker)
				}

		function clearMarker(id) {
		  var iteration = []
		  markers.forEach(function(marker) {
		    if (marker._id == id){
		    	$('#id').val(marker._id);
		    	$('#longitude').val(marker.getLatLng().lng);
		    	$('#latitude').val(marker.getLatLng().lat);
		    } 
		  })
		}
		//Afficher les marqueurs
		var coordinates = <?php echo json_encode($marqueur);?>;
		for (i = 0;i<coordinates.length;i++){
			var coord = []
			coord.push(coordinates[i][1]);
			coord.push(coordinates[i][2]);
			console.log(coordinates[i]);
			createMarker(coord,coordinates[i][0]);
		    	}

		    	map.on('popupopen', function(e){ 
		            var idselected = e.popup._source._id;
		            console.log(idselected);
		             for(var key in databases){

		                if(key == idselected){
		                    console.log(databases[key][0]);
		                    for(j = 0; j<databases[key].length;j++){
		                        $("#database").append(databases[key][j]);
		                    }
		                    break;
		                }
		          }
		          document.getElementById('test').style.display = 'block';
				});

	    map.on('popupclose', function(e){
            document.getElementById('database').innerHTML = "";
	    	document.getElementById('test').style.display = 'none';

		});
	</script>