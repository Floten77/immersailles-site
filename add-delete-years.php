<?php
	if(!(isset($_SESSION['role']))){
		header('location:index.php');
	}
	//On vérifie que l'utilisateur a bien cliqué sur le bouton puor ajouter une année
	if(isset($_POST['ajouter'])){
		//On regarde si l'input n'est pas vide sinon on l'ajoute
		if($_POST['addYear']!=""){
			if(intval($_POST['addYear'])){
				//On regarde si l'année existe
				$verif ='SELECT IDannées FROM années WHERE IDannées = ?';
				$request = $dbh->prepare($verif);
				$request->execute(array($_POST['addYear']));
				$results = $request->fetch();
				if($results['IDannées']){
					echo '<p class="mx-auto" style="width:375px;color:red;">L\'année que vous voulez ajouter existe déjà</p>';
				}
				//Si l'année existe pas
				else{
					//Requete pour insert l'année
					$insert = 'INSERT INTO années VALUES (?,?,?)';
					$request = $dbh->prepare($insert);

					$request->execute(array($_POST['addYear'],$_POST['addYear'],$_SESSION['IDUsers']));
					echo '<p class="mx-auto" style="width:300px;color:green;">L \'année '.$_POST['addYear'].' a bien été ajouté</p>';
				}
			}
			else{
				echo '<p class="mx-auto" style="width:375px;color:red;"> Vous n\'avez pas rentrez une année</p>';
			}
		}
	}
	//Si l'utilisateur a supprimé une année
	if(isset($_POST['supprimer'])){
		//On vérifie qu'elle existe
		$verif = 'SELECT IDannées FROM associer WHERE IDannées = ?';
		$req = $dbh->prepare($verif);
		$req->execute(array($_POST['deleteYear']));
		$results = $req ->rowCount();
		if ($results >0){
			//On supprimer tous les niveaux associer à l'année
			$deleteall = $dbh->prepare('DELETE FROM associer WHERE IDannées = ?');
			$deleteall -> execute(array($_POST['deleteYear']));
		}
		//On supprime l'année
		$delete = 'DELETE FROM années WHERE IDannées = ?';
		$del = $dbh->prepare($delete);
		$del->execute(array($_POST['deleteYear']));
	}
?>
<form style="margin-top:5%" method ="POST" action="addYears.php">
	<div class="mx-auto" style="width: 350px;"><label class="h2">Supprimer une année</label></div>
	<div class="mx-auto" style="width:450px;margin-top:2%;"><label>Selectionner l'année à supprimer
	<select name="deleteYear">
		<?php
		$req = 'SELECT IDannées FROM années';
		$request = $dbh->query($req);
		while($results = $request->fetch()){
			echo '<option value="'.$results['IDannées'].'">'.$results['IDannées'].'</option>';
		}
		?>
	</select>
	</label></div>
	<div class="mx-auto" style="width:250px;margin-top:2%;"><input type="submit" name ="supprimer" value="Confirmer votre demande"/></div>
</form>


<?php
	if(isset($_POST['supprimer'])){
		//Affichage du résultat
		echo '<p class="mx-auto" style="width:320px;">L\'année '.$_POST['deleteYear'].' a bien été supprimé</p>';
	}
	include('footer.php');
?>