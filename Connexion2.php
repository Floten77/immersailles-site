<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Immersailles - Connexion</title>
  </head>
  <body>
    <header class="w-100 bg-dark text-light d-flex justify-content-between">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm">
              <img src="images/logo.png " alt="logoImmersailles">
            </div>
            <div class="col-sm">
              <p class="h1 font-italic mt-4">IMMERSAILLES - CONNEXION</p>
            </div>
            <div class="col-sm">
            </div>
          </div>
        </div>
    </header>
    <h1 class="mx-auto mt-4" style="width: 12%">Connexion</h1>
    <br/><br/>
        <form action="Connexion2.php" method="post">
        <div class="mx-auto" style="width: 12%">
            <label for="login">Entrez votre identifiant : </label>
            <input type="text" name="login" id="login">
            <br/><br/>
            <label for="mdp">Entrez votre mot de passe : </label>
            <input type="password" name="mdp" id="mdp">
            <br/><br/>
            <div style="font-size: 13px"><a href="motdepasse.php">Mot de passe oublié ?</a></div>
            <input class="mr-4 mt-2" type="reset" value="annuler">
            <input type="submit" name="valider" value="valider">
            <br/>
        </div>
        <?php
        //On vérifie si l'utilisateur a bien appuyé sur s'authentifier
          if(isset($_POST["valider"])){
            //Analyse du mot de passe et de l'identifiant et ensuite soit on le dirige vers la page concernée, soit on le prévient par un message que celui-ci est incorrect
              include("login.php");
              if($_POST["mdp"] == ""){
                echo "<center><p class=\"text-danger ml-4\"> Mot de passe non renseigné !</p></center>";
              }
              else if($_POST["login"] == ""){
                echo "<center><p class=\"text-danger ml-4\"> Identifiant non renseigné !</p></center>";
              }
              else{
                echo "<center><p class=\"text-danger ml-4\"> Mot de passe ou identifiant incorrect !</p></center>";
              }
          }


        ?>
        </form>
      


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>