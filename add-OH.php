<?php
    //Pour vérifier si l'utilisateur a bien appuyé sur le bouton
    if(isset($_POST['creer'])){
        //Pour vérifier si l'utilisateur a bien renseigné le nom de l'objet
        if($_POST['nomObjet'] != ""){
            //Pour vérifier si l'utilisateur a bien renseigné l'ID wikidata de l'objet
            if($_POST['IdData'] != ""){
                //Requête pour vérifier si l'objet est déjà présent dans la base de données
                $req = $dbh ->prepare("SELECT NomObjet FROM objethistoriques WHERE NomObjet = :nomObjet");
                $req -> bindParam(':nomObjet', $_POST['nomObjet']);
                $req -> execute();
                $res = $req -> fetch();
                if($res['NomObjet']){
                    //Si c'est le cas, avertir l'utilisateur
                    echo "<center><p class=\"text-danger mt-2\">Cet objet est déjà présent dans la base de données</p></center>";
                }
                //sinon ajouter l'objet dans la base de données
                else{
                    $request = "INSERT INTO objethistoriques VALUES(?,?,?,?,?,?,?,?,?)";
                    $insert = $dbh -> prepare($request);
                    $insert -> execute(array(NULL, $_POST['IdData'], $_POST['nomObjet'], $_POST['TypeObjet'], 
                    $_POST['url1'], $_POST['url2'], $_POST['url3'], $_POST['url4'], $_SESSION['IDUsers']));
                    echo "<center><p class=\"text-success mt-2\">Objet ajouté !</p></center>";
                }
            }
            else{
                //message pour avertir dans le cas où l'utilisateur n'a pas renseigné l'ID wikidata de l'objet
                echo "<center><p class=\"text-danger mt-2\">L'ID WikiData de l'objet n'est pas renseigné !</p></center>";
            }
        }
        else{
            //message pour avertir dans le cas où l'utilisateur n'a pas renseigné le nom de l'objet
            echo "<center><p class=\"text-danger mt-2\">Le nom de l'objet n'est pas renseigné !</p></center>";
        }
    }



    
?>