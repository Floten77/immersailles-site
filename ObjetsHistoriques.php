<?php
    include("header.php");
    include("fonctions.php");
    if(!isset($_SESSION['role'])){
    	header('location:index.php');
    }
?>
<!-- formulaire afin d'ajouter un objet historique dans la base de données -->
<form action="" method="POST">
	<div class="mx-auto mt-3" style="width: 400px;"><label class="h2">Ajouter un objet historique</label></div>
	<div class="mx-auto" style="width:500px;margin-top:2%;"><label>Nom de l'objet : <input type="text" style="margin-left:15px" name="nomObjet"></label></div>
    <div class="mx-auto" style="width:500px;margin-top:2%;"><label>ID Wikidata de l'objet : <input type="text" style="margin-left:15px" name="IdData"></label></div>
    <div class="mx-auto" style="width:500px;margin-top:2%;"><label>Type de l'objet : 
    <select class="ml-3" name="TypeObjet">
		<!-- Requete afin d'afficher les différents types d'objet présents dans la base de données -->
		<?php
		$req = "SELECT DISTINCT TypeObjet FROM objethistoriques";
		$request = $dbh->prepare($req);
		$request->execute();
		while($results = $request->fetch()){
			echo '<option value="'.$results['TypeObjet'].'">'.$results['TypeObjet'].'</option>';
		}
		?>
	</select></label></div>
		<div class="mx-auto" style="width:500px;margin-top:2%;"><label>Lien de site de ressources : <input type="url" style="width:250px;margin-left:15px" placeholder="https://exemple.com (Optionnel)" name="url1"></label></div>
		<div class="mx-auto" style="width:500px;margin-top:2%;"><label>Lien de site de ressources : <input type="url" style="width:250px;margin-left:15px" placeholder="https://exemple.com (Optionnel)" name="url2"></label></div>
		<div class="mx-auto" style="width:500px;margin-top:2%;"><label>Lien de site de ressources : <input type="url" style="width:250px;margin-left:15px" placeholder="https://exemple.com (Optionnel)" name="url3"></label></div>
		<div class="mx-auto" style="width:500px;margin-top:2%;"><label>Lien de site de ressources : <input type="url" style="width:250px;margin-left:15px" placeholder="https://exemple.com (Optionnel)" name="url4"></label></div>
		<div class="mx-auto" style="width:150px;margin-top:2%;margin-bottom:2%"><input type="submit" style="width:150px" value="Créer" name="creer" /></div>
<center></form>
<?php include('add-OH.php'); ?>
<!-- formulaire afin de supprimer un objet historique de la base de données -->
<form style="height:150px" action="" method="POST">
	<div class="mx-auto mt-3" style="width: 500px;"><label class="h2">Supprimer un objet historique</label></div>
	<div class="mx-auto" style="width:400px;margin-top:2%;"><label>Nom de l'objet : 
    <select style="" class="ml-3" name="NomObjetS">
	<?php
		$req = "SELECT NomObjet FROM objethistoriques";
		$request = $dbh->prepare($req);
		$request->execute();
		while($results = $request->fetch()){
			echo '<option value="'.$results['NomObjet'].'">'.$results['NomObjet'].'</option>';
		}
		?>
		</select></label></div>
		<div class="mx-auto" style="width:150px;height:100px;margin-top:2%;margin-bottom:2%"><input type="submit" style="width:150px" value="Supprimer" name="supp" /></div>

</form></center>
<?php
	include("delete-OH.php");
?>