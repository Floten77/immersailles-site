<?php
	include("header.php");
	if(isset($_SESSION['role'])){
		if ($_SESSION['role'] ==0){
			header('location:index.php');
		}		
	}
	else{
		header('location:index.php');
	}
?>
<!-- formulaire pour l'ajout de compte -->
<center><form action="" method="POST">
    <div class="mx-auto mt-3" style="width: 400px;"><label class="h2">Ajouter un compte</label></div>
    <div class="mx-auto" style="width:500px;margin-top:2%;"><label>Login de l'utilisateur : <input type="text" style="margin-left:15px" name="loginuser"></label></div>
    <div class="mx-auto" style="width:500px;margin-top:2%;"><label>adresse e-mail de l'utilisateur : <input type="email" pattern=".+@gmail.com"style="margin-left:15px" name="mailuser"></label></div>
	<div class="mx-auto" style="width:500px;margin-top:2%;"><label>mot de passe de l'utilisateur : 
	<input type="text" style="margin-left:15px" id="mdpuser" name="mdpuser"/></label></div>
	<div class="mx-auto" style="width:500px;margin-top:2%;"><label>Role de l'utilisateur : 
    <select class="ml-3" name="roleuser">
	<option value="Contributeur">Contributeur</option>
	<option value="Administrateur">Administrateur</option>
	</select></label></div>
    <div class="mx-auto" style="width:150px;margin-top:2%;margin-bottom:2%"><input type="submit" style="width:150px" value="Créer" name="creer" /></div>
</form><button onclick="clicked()">Générer mot de passe</button></center>

<?php 
include("add-compte.php");
include("delete-compte.php");
include("edit-compte.php");
?>
<!-- formulaire pour la suppresion de compte -->
<center><form style="height:180px" action="" method="POST">
	<div class="mx-auto mt-3" style="width: 500px;"><label class="h2">Supprimer un Compte</label></div>
	<div class="mx-auto" style="width:400px;margin-top:2%;"><label>Login de l'utilisateur : 
    <select class="ml-3" name="loginS">
	<?php
		//pour afficher les logins de tout les utilisateurs, dans un select
		$req = "SELECT login FROM users";
		$request = $dbh->query($req);
		while($results = $request->fetch()){
			echo '<option value="'.$results['login'].'">'.$results['login'].'</option>';
		}
		?>
		</select></label></div>
        <div class="mx-auto" style="width:150px;margin-top:2%;margin-bottom:2%"><input type="submit" style="width:150px" value="Supprimer" name="supp" /></div>
</form></center>
<?php
	if(isset($_POST['supp'])){
		echo "<center><p class=\"text-success mt-4 mb-2\">Compte supprimé !</p></center>";
	}
?>
<!-- formulaire pour la modification du role de compte -->
<center><form style="height:180px" action="" method="POST">
<div class="mx-auto mt-3" style="width: 500px;"><label class="h2">Modifier un Compte</label></div>
<div class="mx-auto" style="width:400px;margin-top:2%;"><label>Login de l'utilisateur : 
<select class="ml-3" name="loginC">
<?php
	//pour afficher les logins de tous les utilisateurs, dans un select
	$req = "SELECT login FROM users";
	$request = $dbh->query($req);
	while($results = $request->fetch()){
		echo '<option value="'.$results['login'].'">'.$results['login'].'</option>';
	}
	?>
	</select></label></div>
	<div class="mx-auto" style="width:500px;margin-top:2%;"><label>Nouveau rôle de l'utilisateur : 
    <select class="ml-3" name="roleuserC">
	<option value="Contributeur">Contributeur</option>
	<option value="Administrateur">Administrateur</option>
	</select></label></div>
	<div class="mx-auto" style="width:150px;margin-top:2%;margin-bottom:2%"><input type="submit" style="width:150px" value="Modifier" name="modif" /></div>
	</form></center>
<?php
	//pour afficher le message de 'edit-compte.php'
	if(isset($_POST['modif'])){
		echo $res;
	}

	//tableau utilisé pour générer le mot de passe aléatoire
	$characters = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
    , "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script>
    var tabl = <?php echo json_encode($characters)?>;
    console.log(tabl);
    function clicked(){
        var password="";
        for(i=0;i<15;i++){
            if(i%2 == 0){
                password=password +tabl[Math.floor(Math.random() * 52)];
            }
            else{
                password=password +tabl[Math.floor(Math.random() * 52)];
            }
        }
        $('#mdpuser').val(password);
    }
</script>

