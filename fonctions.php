<!doctype html>
<html>
<head>
</head>
<body>
    <?php
        //return le role de l'utilisateutr connecté sur sa session
        function isUser($dbh, $login, $pwd) {
            $requete = $dbh->prepare("SELECT IDUsers, login, password, role FROM `users` WHERE login = :login");
            $requete->bindParam(':login', $login);
            $requete->execute();
            $t = $requete->fetch();
            if(password_verify($pwd, $t['password'])){
                if($t['role'] == 1){              //si l'utilisateur est un admin
                    $_SESSION['role'] = $t['role'];
                    return 'Admin';
                }
                else if($t['role'] == 0){
                    $_SESSION['role'] = $t['role'];   
                    return 'Contributeur';          //si l'utilisateur est un contributeur
                }
            }
            return "";                              //si ce n'est pas un utilisateur
        }

        //renvoie le lien json lié à l'ID wikidata de l'objet
        function getlinkJson($IdData){
            return "https://www.wikidata.org/wiki/Special:EntityData/".$IdData.".json";
        }

        //renvoie l'ID Wikidata de l'objet à partir de la base de données
        function getIdData($dbh, $nomObjet){
            $req = $dbh -> prepare('SELECT IDdata FROM objethistoriques WHERE NomObjet = :nomObjet');
            $req -> bindParam(':nomObjet', $nomObjet);
            $req -> execute();
            $resreq = $req -> fetch();
            $IdData = $resreq['IDdata'];
            return $IdData;
        }

        //permet de transformer le format json en objet, que l'on peut désormais parcourir
        function getObjectDecoded($dbh, $nomObjet){
            $IdData = getIdData($dbh, $nomObjet);
            $filename = getlinkJson($IdData);
            // Création d'un flux
            $opts = array(
              'http'=>array(
                'method'=>"GET",
                'header'=>"Accept-language: en\r\n" .
                          "Cookie: foo=bar\r\n"
              )
            );

            $context = stream_context_create($opts);
            $idata = $dbh->prepare('SELECT IDdata FROM objethistoriques WHERE NomObjet ="'.$nomObjet.'"');
            $idata->execute();
            $results = $idata->fetch();
            // Accès à un fichier HTTP avec les entêtes HTTP indiqués ci-dessus
            $opendata = file_get_contents('https://www.wikidata.org/wiki/Special:EntityData/'.$results['IDdata'].'.json', false, $context);
            $object = json_decode($opendata);
            return $object;
        }

        //renvoie le type de l'objet en question
        function getTypeObject_Aux($dbh, $nomObjet){
            $req = $dbh -> prepare('SELECT TypeObjet FROM objethistoriques WHERE NomObjet = :nomObjet');
            $req -> bindParam(':nomObjet', $nomObjet);
            $req -> execute();
            $resreq = $req -> fetch();
            $type = $resreq['TypeObjet'];
            return $type;
        }

        //renvoie le code permettant l'affichage html du type de l'objet sur le site
        function getTypeObject($dbh, $nomObjet){
            $type = getTypeObject_Aux($dbh, $nomObjet);
            return "<p class=\"mt-5 text-white\" ><span class=\"text-warning h5\" >Type d'objet : </span>".$type."</p>";
        }

        //renvoie true si l'objet est un personnage (à partir de la base de données)
        function isPersonnage($dbh, $nomObjet){
            $type = getTypeObject_Aux($dbh, $nomObjet);
            if($type == 'Personnage'){
                return 1;
            }
            else{
                return 0;
            }
        }

        //renvoie true si l'objet est un mobilier (à partir de la base de données)
        function isMobilier($dbh, $nomObjet){
            $type = getTypeObject_Aux($dbh, $nomObjet);
            if($type == "Mobilier"){
                return 1;
            }
            else{
                return 0;
            }
        }

        //renvoie true si l'objet est une oeuvre (à partir de la base de données)
        function isOeuvre($dbh, $nomObjet){
            $type = getTypeObject_Aux($dbh, $nomObjet);
            if($type =="Oeuvre"){
                return true;
            }
            else{
                return 0;
            }
        }

        //renvoie le code permettant l'affichage du nom de l'objet dans le site, à partir des données wikidata
        function giveValueName($dbh, $nomObjet,$object){
            $IdData = getIdData($dbh, $nomObjet);
            $res = $object->entities->$IdData->labels->fr->value;
           return "<p class=\"mt-5 text-white\"><span class=\"text-warning h5\" >Nom : </span>".$res."</p>";
        }

        //renvoie le code permettant l'affichage de la date de naissance
        //(si c'est un personnage) ou de création de l'objet dans le site, à partir des données wikidata
        function giveValueDateOfCreation($dbh, $nomObjet,$object){
            $IdData = getIdData($dbh, $nomObjet);
            if(isPersonnage($dbh, $nomObjet)){
                $res = $object->entities->$IdData->claims->P569[0]->mainsnak->datavalue->value->time;
                $resf = date_format(date_create($res), "d/m/Y");
                return "<p class=\"mt-5 text-white\"><span class=\"text-warning h5\" >Date de naissance : </span>".$resf."</p>";
            }
            else{
                $res = $object->entities->$IdData->claims->P571[0]->mainsnak->datavalue->value->time;
                $resf = date_format(date_create($res), "d/m/Y");
                return "<p class=\"mt-5 text-white\"><span class=\"text-warning h5\" >Date de création : </span>".$resf."</p>";
            }
        }

        //renvoie le code permettant l'affichage de la date de mort
        //du personnage dans le site, à partir des données wikidata, rien sinon
        function giveValueDateOfDeath($dbh, $nomObjet,$object){
            $IdData = getIdData($dbh, $nomObjet);
            $res = $object->entities->$IdData->claims->P570[0]->mainsnak->datavalue->value->time;
            $resf = date_format(date_create($res), "d/m/Y");
            if(isPersonnage($dbh, $nomObjet)){
                return "<p class=\"mt-5 text-white\"><span class=\"text-warning h5\" >Date de mort : </span>".$resf."</p>";
            }
            else{
                return "";
            }
        }

        //renvoie le code permettant l'affichage de la description de l'objet,
        //à partir des données wikidata
        function giveDescription($dbh, $nomObjet,$object){
            $IdData = getIdData($dbh, $nomObjet);
            $res = $object->entities->$IdData->descriptions->fr->value;
            return "<p class=\"mt-5 text-white\"><span class=\"text-warning h5\" >Description : </span>".$res."</p>";
        }

        //renvoie le Nom de l'objet nécessaire dans le lien de l'image
        function giveValuePhoto($dbh, $nomObjet,$object){
            $IdData = getIdData($dbh, $nomObjet);
            $res = $object->entities->$IdData->claims->P18[0]->mainsnak->datavalue->value;
            return $res;
        }

        //renvoie le code permettant l'affiche de l'image, à partir du lien
        function givePhotoObject($dbh, $size, $nomObjet,$object){
            
            $res = giveValuePhoto($dbh, $nomObjet,$object);
            if($res != ""){
                $res = str_replace(" ", "_", $res);
                $resmd5 = md5($res);
                $headlink = "https://upload.wikimedia.org/wikipedia/commons/thumb";
                $md5link = "/".$resmd5[0]."/".$resmd5[0].$resmd5[1]."/";
                $sizephoto = "/".$size."px-".$res;      //pour paramétré la taille de l'image
                $link = $headlink.$md5link.$res.$sizephoto;
                return '<img src="'.$link.'" />';
            }
            else{
                return;
            }

        }
?>
</body>
</html>
