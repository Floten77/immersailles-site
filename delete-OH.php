<?php
    //Pour vérifier si l'utilisateur a bien appuyé sur le bouton
    if(isset($_POST['supp'])){
        //requete pour supprimer l'objet de la base de données
        $req = $dbh -> prepare("DELETE FROM objethistoriques WHERE NomObjet = :nomObjet");
        $req -> bindParam(':nomObjet', $_POST['NomObjetS']);
        $req -> execute();
        //message de succès
        echo "<center><p class=\"text-success mt-5\">Objet supprimé !</p></center>";

    }
?>