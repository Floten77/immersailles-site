<?php
include("header.php");
    //Pour vérifier si l'utilisateur a bien appuyé sur le bouton
    if(isset($_POST['valider'], $_POST['mail'])){
        //Pour vérifier si l'utilisateur a bien renseigné son adresse email
        if($_POST['mail'] != ""){
            //Pour convertir les caractères spéciaux en balise HTML
            $mail = htmlspecialchars($_POST['mail']);
            //si le mail est au bon format
            if(filter_var($mail,FILTER_VALIDATE_EMAIL)) {
                //Pour vérifier si cette adresse correspond bien à un utilisateur dans la base de données
                $req = $dbh -> prepare("SELECT * FROM users WHERE mail = :mail");
                $req -> bindParam(':mail', $_POST['mail']);
                $req -> execute();
                $t = $req ->fetch();
                //si c'est le cas, on créer le mail avec le lien afin de modifier son mot de passe, puis on l'envoie
                if($t["IDUsers"]){
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $subject = "Changement de votre mot de passe - Immersailles";
                    $lien = "<a href=\"http://perso-etudiant.u-pem.fr/~fngor/immersailles-site/newpassword.php?IDUsers=".$t['IDUsers']."\">votrelien</a>";
                    $message = "Bonjour ".$t['login'].",<br/><br/>Voici le lien afin de modifier votre mot de passe : ".$lien."<br/><br/>Cordialement,<br/>Immersailles";
                    $corps="<HTML><BODY><FONT FACE='Arial, Verdana' SIZE=2>";
	                $corps.=$message."</BODY></HTML>";
                    mail($mail, $subject, $corps, $headers);
                    echo "<center><p class=\"text-success mt-2\">Mail envoyé !</p></center>";
                    //header("location:newpassword.php?IDUsers=".$t['IDUsers']);
                }
                else{
                    //si l'utilisateur a renseigné une adresse email non présente dans la base de données
                    echo "<center><p class=\"text-danger mt-2\"> Adresse mail introuvable !</p></center>";
                }
            }
            else{
                 //message pour avertir dans le cas où l'utilisateur n'a pas renseigné une adresse mail au bon format
                echo "<center><p class=\"text-danger mt-2\"> Veuillez entrer une adresse mail valide !</p></center>";
            }
        }
        else{
             //message pour avertir dans le cas où l'utilisateur n'a pas renseigné son adresse email
            echo "<center><p class=\"text-danger mt-2\"> Veuillez entrer votre adresse mail ! </p></center>";
        }
        
    }
?>
<center><form class="cssform" action="motdepasse.php" method="post">
    <label class="h5" for="mail">Entrez votre adresse mail reliée au compte : </label>
    </br></br>
    <input type="text" name="mail" id="mail">
    <input class="buttoncss" type="submit" name="valider" value="valider">
    <br/></br>
</form></center>


<?php
include("footer.php");
?>