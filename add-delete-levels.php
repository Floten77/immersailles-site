<?php 
	if(!(isset($_SESSION['role']))){
		header('location:index.php');
	}
	include('header.php');
?>
<form action="add-delete-levels.php" method="POST">
	<div class="mx-auto" style="width: 300px;"><label class="h2">Ajouter des niveaux</label></div>
	<div class="mx-auto" style="width:320px;margin-top:2%;"><label>Choisir le niveau<select name="selectedlevels" style="margin-left:80px">
	<?php
		$req = 'SELECT * FROM niveaux';
		$request = $dbh->query($req);
		while($results = $request -> fetch()){
			echo '<option value="'.$results['IdLieu'].'">'.$results['nomLieu'].'</option>';
		}
	?>
	</label></select></div>
	<div class="mx-auto" style="width:320px;"><label>Choisir l'époque à affecter<select name="selectedYears" style="margin-left:10px">
	<?php
	//On selectionne tous les niveaux
		$req ='SELECT * FROM années';
		$request = $dbh->query($req);
		while($results = $request -> fetch()){
			echo '<option value="'.$results['IDannées'].'">'.$results['IDannées'].'</option>';
		}
	?>
	</select></label></div>
	<div class="mx-auto" style = "width:200px"><input type="submit" name="Add"value="Valider votre demande"/></div>

	<?php
	//On regarde si l'utilisateur veut ajouter un niveau pour une année
	if(isset($_POST['Add'])){
		//Requete pour voir s'il existe ou non
		$verif ='SELECT * FROM associer WHERE IDannées = ? AND IdLieu = ?';
		$req = $dbh->prepare($verif);
		$req->execute(array($_POST['selectedYears'],$_POST['selectedlevels']));
		$results = $req->fetch();
		//Il existe
		if($results['IDannées']){
			echo "<p class='mx-auto' style='width:300px'>Il y'a déjà ce niveau affecté à ce niveau</p>";
		}
		//Il n'existe pas
		else{
			$request = "INSERT INTO associer VALUES(?,?)";
			$insert = $dbh->prepare($request);
			$insert->execute(array($_POST['selectedYears'],$_POST['selectedlevels']));
			echo '<p class="mx-auto" style="width:300px">Votre demande a bien été ajouté à la BDD </p>';
		}

	}
	?>
</form>

<?php
//On vérifie si l'utilisateur a bien cliqué sur le bouton changer de plan pour un niveau
if(isset($_POST['uploader'])){
	//On selectionne la carte actuellement en affectation au niveau
    	$variable = explode("-",$_POST['editedlevel']);
    	$requete = "SELECT Cartes FROM niveaux WHERE IdLieu = ?";
    	$req = $dbh->prepare($requete);
    	$req->execute(array($variable[1]));
    	$results = $req->fetch();
    	//Si elle existe alors on la supprime
    	if($results['Cartes'] != "" && isset($_POST['filename'])){
    		unlink("reconnaissance/affectation/".$results['Cartes']);
    	}
    	//Si l'image selectionné existe bien dans upload alors on ajoute
    	if(isset($_POST['filename'])){
    		$update = "UPDATE niveaux SET Cartes = ? WHERE IdLieu = ? ";
    		$updating = $dbh->prepare($update);
    		$updating->execute(array($_POST['filename'],$variable[1]));
			rename("admin/upload/".$_POST['filename'],"reconnaissance/affectation/".$_POST['filename']);
    	}
    	else{
    		'<center><p>Il n y a pas d images selectionné</p></center>';
    	}

	}
?>
<center><form class="mt-5" action="add-delete-levels.php" method="POST">
<div><label class="h2" style="margin-left:-2%">Editer un niveau</label></div>
<select name="editedlevel">
		<?php
		$req = 'SELECT IDannées,associer.IdLieu,nomlieu FROM associer,niveaux WHERE associer.IdLieu = niveaux.IdLieu';
		$request = $dbh->query($req);
		while($donnees = $request->fetch()){
			echo "<option value =".$donnees['IDannées']."-".$donnees['IdLieu'].">".$donnees['IDannées']."-".$donnees['nomlieu']."</option>";
		}
		?>
	</select>
<select name="filename" value="">
    <?php
    //On selectionne tous les images présents 'admin/upload'
if($dossier = opendir('admin/upload')){
	$nb =0;
	while(false !== ($fichier = readdir($dossier))){
		if($nb >1){
			echo '<option value="' . $fichier . '">' . $fichier . '</option>';
			}
		$nb++;
	}
}
?>
</select>
<input type="submit" name="uploader"/>
</form></center>


<?php
	//Message de resultats après edit d'un niveau
	if(isset($_POST['uploader']) && isset($_POST['filename'])){
		if($_POST['filename']){
			echo "<center><p>".$_POST['filename']. " a bien été affecté à l année ".$variable[0]." pour le niveau IDLieu ".$variable[1]."</p></center>";
		}
	}
	else{
		if(isset($_POST['uploader'])){
			echo "<center><p>L'opération a échoué parce qu'il n'y'a plus de fichier dans le dossier upload, veuillez remettre des images pour pouvoir réuploader à nouveau</p></center>";
		}
		
	}
	//On vérifie si l'utilisateur a bien appuyé sur le bouton supprimer et on le supprimer avec la requete DELETE
	if(isset($_POST['delete'])){
		$variable = explode("-",$_POST['deletedlevel']);
		$req = "DELETE FROM associer WHERE IDannées = ? AND IdLieu = ?";
		$request = $dbh->prepare($req);
		$request -> execute(array($variable[0],$variable[1]));
	}
?> 
<form style="margin-top:4%" method="POST" action = "">
	<div class="mx-auto" style="width: 350px;"><label class="h2">Supprimer des niveaux</label></div>
	<div class="mx-auto" style="width:370px;"><label>Choisir le niveau à supprimer</label>
	<select name="deletedlevel">
		<?php
		$req = 'SELECT IDannées,associer.IdLieu,nomlieu FROM associer,niveaux WHERE associer.IdLieu = niveaux.IdLieu';
		$request = $dbh->query($req);
		while($donnees = $request->fetch()){
			echo "<option value =".$donnees['IDannées']."-".$donnees['IdLieu'].">".$donnees['IDannées']."-".$donnees['nomlieu']."</option>";
		}
		?>
	</select>

	</div>
	<div class="mx-auto" style="width:150px"><input type="submit" name="delete" value="Supprimer"></div>
</form>
<?php 
?>

