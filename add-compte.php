<?php
    //pour vérifier si l'utilisateur a bien appuyé sur le bouton
    if(isset($_POST['creer'])){
        //Pour vérifier si l'utilisateur a bien renseigné le login du nouvel utilisateur
        if($_POST['loginuser'] != ""){
            //Pour vérifier si l'utilisateur a bien renseigné l'adresse email du nouvel utilisateur
            if($_POST['mailuser'] != ""){
                //Pour vérifier si l'utilisateur a bien renseigné ou générer le mot de passe du nouvel utilisateur
                if($_POST['mdpuser'] != ""){
                    //Requête pour vérifier si le login rentrez n'existe pas déjà dans la base de données
                    $req1 = $dbh ->prepare("SELECT login FROM users WHERE login = :login");
                    $req1 -> bindParam(':login', $_POST['loginuser']);
                    $req1 -> execute();
                    $res1 = $req1 -> fetch();
                    //Requete pour vérifier si le mail n'existe pas déjà dans la base de données
                    $req2 = $dbh ->prepare("SELECT mail FROM users WHERE mail = :mail");
                    $req2 -> bindParam(':mail', $_POST['mailuser']);
                    $req2 -> execute();
                    $res2 = $req2 -> fetch();
                    
                    if($_POST['roleuser'] == "Contributeur"){
                        $role = 0;
                    }
                    else{
                        $role = 1;
                    }
                    //si c'est le cas, avertir l'utilisateur, sinon, ajouté le compte à la base de données
                    if($res1['login']){
                        echo "<center><p class=\"text-danger mt-2\">Ce login est déjà utilisé.</p></center>";
                    }
                    else if($res2['mail']){
                            echo "<center><p class=\"text-danger mt-2\">Ce mail est déjà utilisé.</p></center>";
                    }
                    else{
                        $request = "INSERT INTO users VALUES(?,?,?,?,?)";
                        $insert = $dbh -> prepare($request);
                        $insert -> execute(array(NULL, $_POST['loginuser'], $_POST['mailuser'], password_hash($_POST['mdpuser'], PASSWORD_BCRYPT), $role));
                        //Pour convertir les caractères spéciaux en balise HTML
                        $mail = htmlspecialchars($_POST['mailuser']);
                        //si le mail est au bon format
                        if(filter_var($mail,FILTER_VALIDATE_EMAIL)) {
                            $headers  = 'MIME-Version: 1.0' . "\r\n";
                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                            $subject = "Inscription dans notre site, Vous voulez changer de mot de passe ? - Immersailles";
                            $lien = "<a href=\"http://perso-etudiant.u-pem.fr/~fngor/immersailles-site/newpassword2.php?login=".$_POST['loginuser']."\">votrelien</a>";
                            $message = "Bonjour ".$_POST['loginuser'].",<br/><br/>";
                            $message .= "Voici votre mot de passe actuel : ".$_POST['mdpuser'];
                            $message .= "<br/>Voici le lien si vous voulez modifier votre mot de passe : ".$lien."<br/><br/>Cordialement,<br/>Immersailles";
                            $corps="<HTML><BODY><FONT FACE='Arial, Verdana' SIZE=2>";
                            $corps.=$message."</BODY></HTML>";
                            mail($mail, $subject, $corps, $headers);
                            echo "<center><p class=\"text-success mt-2\">Compte ajouté, le nouvel utilisateur va recevoir un mail s'il souhaite modifier son mot de passe !</p></center>";
                        }
                        
                            
                        
                    }
                }
                //si l'utilisateur n'a pas renseigné ou générer le mot de passe du nouvel utilisateur
                else{
                    echo "<center><p class=\"text-danger mt-2\">Le mot de passe de l'utilisateur n'est pas renseigné !</p></center>";
                }   
            }
            //si l'utilisateur n'a pas renseigné l'adresse e-mail du nouvel utilisateur
            else{
                echo "<center><p class=\"text-danger mt-2\">L'adresse e-mail de l'utilisateur n'est pas renseigné !</p></center>";
            }
        }
        //si l'utilisateur n'a pas renseigné le login du nouvel utilisateur
        else{
            echo "<center><p class=\"text-danger mt-2\">Le login de l'utilisateur n'est pas renseigné !</p></center>";
        }
    }