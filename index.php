<?php
include ('header.php');
require ('fonctions.php');
?>

 <div class="w-100 d-flex border border-black m-0" style="height:50px">
      <div class="d-flex">
        <div class="d-flex h5 ml-2 mb-0"><p class="ml-5">Trier</p><p class="text-secondary ml-2">:</p>
        </div>
        <div class="d-flex mb-5">
          <ul class="d-flex ml-3 mr-5">
            <li class="list-unstyled mr-3">Tout (330) </li>
            <li class="list-unstyled mr-3">Oeuvres d'art (120)</li>
            <li class="list-unstyled mr-3">Mobilier (100)</li>
            <li class="list-unstyled mr-3">Décoration (110)</li>
          </ul>
        </div>
      </div>
    </div>

    <div class="d-flex flex-row" style="height:50%">
       <div id="map" style="margin-left:10%;width:75%; height: 100%;">
       </div>
		<?php 
        //$marqueur va contenir les marqueurs correspondant 
	$marqueur = array();
    //On effectue la carte pour avoir la carte
     $url = $dbh->prepare('SELECT Cartes FROM niveaux,associer WHERE associer.IdLieu = niveaux.IdLieu AND IDannées = ?');
     //On verifie si oui ou non l'année a déjà été selectionnée
    if(isset($_GET['year'])){
         $url -> execute(array($_GET['year']));
    }
    else{
        $url -> execute(array(1704));
    }
    $res = $url->fetch();
    //On stocke la carte dans $tabl
    $tabl = array();
    //On recupère le carte à partir de la requete
    if($res['Cartes'] !=""){
        array_push($tabl,$res['Cartes']);
    }
    else{
        array_push($tabl,"");
    }
    //Requete pour avoir tous les marqueurs correspondant au niveau
    $request = $dbh->prepare('SELECT * FROM marqueur WHERE IdLieu = ? ');
    //On verifie si un niveau ou une année a été selectionnée
    if(isset($_POST['level']) || isset($_GET['year'])){
        //Si une année est selectionnée on affiche les marqueurs correspondant à cette année
        if(isset($_GET['year'])){
           $defniveaux=$dbh->prepare( 'SELECT * FROM `associer`,niveaux WHERE associer.IdLieu = niveaux.IdLieu AND IDannées = :annees');
           $defniveaux->execute(array($_GET['year']));
           $resultats = $defniveaux->fetch();
           if(isset($resultats['IdLieu'])){
            $request->execute(array($resultats['IdLieu']));
            $levelselect = $resultats['IdLieu'];
           }
           //On affice les marqueurs du RDC
           else{
            $request->execute(array(101));
            $levelselect = 101;
           }
        }
        //On recupère les aarqueurs de l'année selectionnée
        else{
            $request ->execute(array($_POST['level']));
             $levelselect = $_POST['level'];
        }
    }
    //Si aucun niveau est selectionnée ou année on affiche les marqueurs du RDC
    else{
        $request ->execute(array(101));
        $levelselect =101;
    }
    //On recupère les marqueurs qui vont être affichées dans l'année et le niveau selectionnée
    while($donnees = $request ->fetch()){
        $position = array();
        array_push($position,intval($donnees['IDmarqueur']));
        array_push($position,doubleval($donnees['x']));
        array_push($position,doubleval($donnees['y']));
        array_push($marqueur,$position);
    }
    $database = array();
    $req = "SELECT IDmarqueur,marqueur.IDObject,NomObjet FROM marqueur,objethistoriques WHERE marqueur.IDObject = objethistoriques.IDObject AND IdLieu = ".$levelselect;
    $answer  = $dbh -> prepare($req);
    $answer ->execute();
    while ($data = $answer ->fetch()){
        $datatable = array();
        //On recupère les informations dans le wikidata
       if(isPersonnage($dbh,$data['NomObjet'])){
           $info = getObjectDecoded($dbh,$data['NomObjet']);
            array_push($datatable,givePhotoObject($dbh, 250, $data['NomObjet'],$info));
            array_push($datatable,getTypeObject($dbh, $data['NomObjet']));
            array_push($datatable ,giveValueName($dbh,$data['NomObjet'],$info));
            array_push($datatable ,giveValueDateOfCreation($dbh,$data['NomObjet'],$info));
            array_push($datatable ,giveValueDateOfDeath($dbh,$data['NomObjet'],$info));
            array_push($datatable ,giveDescription($dbh,$data['NomObjet'],$info));
        
        }
        if(isOeuvre($dbh,$data['NomObjet'])){
             $info = getObjectDecoded($dbh,$data['NomObjet']);
            array_push($datatable,givePhotoObject($dbh, 250, $data['NomObjet'],$info));
           array_push($datatable,giveValueName($dbh,$data['NomObjet'],$info));
           array_push($datatable,giveValueDateOfCreation($dbh, $data['NomObjet'],$info));
           array_push($datatable,giveDescription($dbh, $data['NomObjet'],$info));
        }
        if(isMobilier($dbh,$data['NomObjet'])){
            $info = getObjectDecoded($dbh,$data['nomObjet']);
            array_push($datatable,givePhotoObject($dbh,250,$data['NomObjet'],$info));
            array_push($datatable,giveValueName($dbh,$data['NomObjet'],$info));
           array_push($datatable,giveValueDateOfCreation($dbh, $data['NomObjet'],$info));
           array_push($datatable,giveDescription($dbh, $data['NomObjet'],$info));
        }

    $database[$data['IDmarqueur']] = $datatable;    
    }   
	?>
	<section class="w-25 bg-dark" style="height:100% ;display:none; overflow-y: scroll;" id ="test">	
       <div class="h-75 d-flex flex-column " id="database">
        </div>
    </section>
        </div>
        <section class="d-flex justify-content-center">
    	<div class="d-flex" style="width:490px; height:75px;overflow-x:auto">
    		<?php
            //Affiche la timeline
    			$request = $dbh->prepare('SELECT * FROM années');
    			$iteration = $request->rowCount();
    			$ite = 1;
                $request ->execute();
    			while($results = $request -> fetch(PDO::FETCH_ASSOC)){
    				if($ite == $iteration){?>
    					<div style="width:50px">
				    		<span style="margin-left:-3px"><?php echo $results['IDannées'] ?></span>
				    		<div class="d-flex flex-row" style="">
					    	<a href="index.php?year=<?php echo $results['IDannées']; ?>"><div style="width:15px;height:15px;border:black 1px solid;border-radius:20px"></div></a>	
					    	</div>
			    		</div>

    				<?php } 
    					else{
    						?>
    			<div style="width:50px">
	    		<span style="margin-left:-2px"><?php echo $results['IDannées'] ?></span>
	    		<div class="d-flex flex-row ">
		    		<a href="index.php?year=<?php echo $results['IDannées']; ?>"><div style="width:15px;height:15px;border:black 1px solid;border-radius:20px"></div></a>	
		    		<div class="mt-2"style="margin-left:2px;border-top:1px solid;width:50px"></div>
	    		</div>
    		</div>
    			<?php }
    			$ite++;
    		}
    		?>
    					
    	


    	</div>
    	<div>
    		<?php
            //Affiche l'année la plus basse lorsqu'on arrive sur le site
    			if(!isset($_POST['levelChange']) && !(isset($_GET['year']))){
    					$request = "SELECT MIN(IDannées) as minyear FROM années";
    					$req = $dbh->prepare($request);
                        $req->execute();
    					$results = $req->fetch();
    					$year = $results['minyear'];
    				}
    				else{
    					$year = $_GET['year'];
    				}
    		
    				?>
    		<form action="index.php?year=<?php echo $year ?>" method="POST">
    			<select name="level">
    			
    			<?php
                // Ici on recupère les niveaux qui peuvent être selectionnées par l'année selectionnée auparavant
					$request2 = 'SELECT niveaux.IdLieu,nomLieu FROM niveaux, associer WHERE IDannées = ? AND niveaux.IdLieu = associer.IdLieu ';
					$req2 = $dbh->prepare($request2);
                    //Si on a déjà choisit une année
					if(isset($_GET['year'])){
							$req2->execute(array($_GET['year']));
						}
					else{
						$req2->execute(array($results['minyear']));
					}
				//Affichage des niveaux
					while($data = $req2->fetch()){
						echo '<option value="'.$data['niveaux.IdLieu'].'">'.$data['nomLieu'].'</option>';
					}
    			?>

    		</select>
    		<input type="submit" name="changeLevel" value="Changer"/>
    		</form>
    	</div>
    	</section>


<?php
include('footer.php');
?>

 <script>
  var markers = []
        var databases = <?php echo json_encode($database); ?>;
       var map = L.map('map', {
        crs: L.CRS.Simple
        });
        var bounds = [[-26.5,-25], [1021.5,1023]];
        if(<?php echo json_encode($tabl[0]); ?> !=""){
             var url = "reconnaissance/affectation/"+<?php echo json_encode($tabl[0]); ?>;
        }
        else{
            var url="images/reconstitue.png";
        }
       
        console.log(url);

        var image = L.imageOverlay(url, bounds).addTo(map);
        map.fitBounds(bounds);
        map.setView([328.505, 486], 1);
        function createMarker(coords,ids) {
          var id
          id =ids
          var popupContent =
            '<p>Some Infomation</p></br>' +
            '<p>test</p></br>';
          myMarker = L.marker(coords, {
            draggable: false
          });
          myMarker._id = id
          var myPopup = myMarker.bindPopup(popupContent, {
            closeButton: false
          });
          map.addLayer(myMarker)
          markers.push(myMarker)
        }
        var coordinates = <?php echo json_encode($marqueur);?>;
        for (i = 0;i<coordinates.length;i++){
        var coord = []
        coord.push(coordinates[i][1]);
        coord.push(coordinates[i][2]);
        console.log(coordinates[i]);
        createMarker(coord,coordinates[i][0]);
            }
        map.on('popupopen', function(e){ 
            var idselected = e.popup._source._id;
            console.log(idselected);
             for(var key in databases){

                if(key == idselected){
                    console.log(databases[key][0]);
                    for(j = 0; j<databases[key].length;j++){
                        $("#database").append(databases[key][j]);
                    }
                    break;
                }
          }
            document.getElementById('test').style.display = 'block';
        });
        map.on('popupclose', function(e){
            document.getElementById('database').innerHTML = "";
            document.getElementById('test').style.display = 'none';
            });
 </script>