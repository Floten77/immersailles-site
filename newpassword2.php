<?php
include("header.php");
?>
<!-- Formulaire de création de nouveau mot de passe -->
<center><form class="mt-5" action="newpassword2.php?login=<?php echo $_GET['login']?>" method="post">
    <label class="h5" for="mdp">Entrez votre mot de passe actuel : </label>
    <input type="password" name="mdp1" id="mdp1">
    </br>
    <label class="h5" for="mdp">Entrez votre nouveau mot de passe : </label>
    <input type="password" name="mdp" id="mdp">
    </br>
    <label class="h5" for="mail">Validez votre nouveau mot de passe : </label>
    <input type="password" name="mdp2" id="mdp2">
    </br>
    <input class="mt-2" type="submit" name="valider" value="valider">
    <br/></br>
</form></center>

<?php
    if(isset($_POST['valider'], $_POST['mdp'], $_POST['mdp2'])){
        //Pour vérifier si l'utilisateur a bien renseigné son nouveau mot de passe
        if($_POST['mdp'] != ""){
            //Pour vérifier si l'utilisateur a bien vérifié son nouveau mot de passe
            if($_POST['mdp2'] != ""){
                if($_POST['mdp1'] != ""){
                    //Pour vérifier si l'utilisateur a bien renseignée deux fois le même mot de passe
                    if($_POST['mdp'] == $_POST['mdp2']){
                        $newmdp = password_hash($_POST['mdp'], PASSWORD_BCRYPT);
                        $req = $dbh -> prepare("UPDATE users SET password = :mdp WHERE login= :login");
                        $req -> bindParam(':mdp', $newmdp);
                        $req -> bindParam(':login', $_GET['login']);
                        $req -> execute();
                        echo "<center><p class=\"text-success mt-2\">Votre mot de passe a bien été modifié !</p></center>";
                        //lien pour retourner à la page de connexion
                        echo "<center><a href=\"Connexion2.php\">Cliquez ici pour retourner à la page de connexion</a></center>";
                        //pour crypté le mot de passe que l'on va ajouter dans la base de données
                    }
                    else{
                        //message pour avertir dans le cas où l'utilisateur n'a pas renseigné deux fois le même mot de passe
                        echo "<center><p class=\"text-danger mt-2\">Mot de passe invalidé !</p></center>";
                    }
                }
                else{
                    //message pour avertir dans le cas où l'utilisateur n'a pas renseigné son mot de passe actuel
                    echo "<center><p class=\"text-danger mt-2\">Veuillez entrer votre mot de passe actuel !</p></center>";
                }
            }
            else{
                //message pour avertir dans le cas où l'utilisateur n'a pas confirmer son mot de passe
                echo "<center><p class=\"mt-2\">Veuillez confirmer votre nouveau mot de passe !</p></center>";
            }
        }
        else{
            //message pour avertir dans le cas où l'utilisateur n'a pas renseigné de nouveau mot de passe
            echo "<center><p class=\"mt-2\">Veuillez entrer votre nouveau mot de passe !</p></center>";
        }
    }


include("footer.php");
?>