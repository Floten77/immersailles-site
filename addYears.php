<?php 
	if(!isset($_SESSION['role'])){
		header('location:index.php');
	}
	include('header.php');
?>

<form action="addYears.php" method="POST">
	<div class="mx-auto" style="width: 350px;"><label class="h2">Ajouter des années</label></div>
	<div class="mx-auto" style="width:450px;margin-top:2%;"><label>Ecrire l'année à ajouter<input type="text" style="margin-left:20px" name="addYear"></label></div>
	<div class="mx-auto" style="width:250px;margin-top:2%;"><input type="submit" value="Confirmer votre demande" name="ajouter" /></div>
</form>

<?php
	include('add-delete-years.php');
?>