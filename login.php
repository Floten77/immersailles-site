<!doctype html>
<html>
<head>
</head>
<body>
    <?php
    include("Connexion.php");
    include("fonctions.php");
    if (isset($_POST['login']) && isset($_POST['mdp'])) {
        $user = isUser($dbh, $_POST['login'], $_POST['mdp']);
        if ($user != "") {
            $_SESSION['login'] = $_POST['login'];
            $req = 'SELECT IDUsers FROM users WHERE login = ?';
            $request = $dbh->prepare($req);
            $request -> execute(array($_POST['login']));
            $results = $request ->fetch();
            $_SESSION['IDUsers'] = $results['IDUsers'];
            $_SESSION['role'] = isUser($dbh, $_POST['login'], $_POST['mdp']);   // Soit il retourne "Contributeur" ou "Admin"
            if ($_SESSION['role'] == "Admin") {
                header('location:admin.php');
            }
            else if ($_SESSION['role'] == "Contributeur") {
                header('location:contributeur.php');
            }
        }
    }
    ?>
</body>
</html>