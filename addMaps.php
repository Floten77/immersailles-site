<?php 
if(!(isset($_SESSION['login']))){
  header('location:index.php');
}
  include("header.php");
?>

<center><form method="post" enctype="multipart/form-data">
<div><label class="h2">Uploader une map sur le site</label></div>
 <input type="file" name="photo">
 <input type="submit">
 </form>
    <?php
    if (isset($_FILES['photo']['tmp_name'])) {
      $sep = explode(".",$_FILES['photo']['name']);
      $size = getimagesize($_FILES['photo']['tmp_name']);
      //Copie de l'image vers /admin/upload en PNG
      if($sep[1] == "png"){
        $source = imagecreatefrompng($_FILES['photo']['tmp_name']);
        $thumb = imagecreatetruecolor(1025, 365);
         imagecopyresized($thumb, $source, 0, 0, 0, 0, 1025,365, $size[0], $size[1]);
         imagepng($thumb,"admin/upload/".$_FILES['photo']['name']);
      }
      //Copie de l'image vers /admin/upload en JPG
      else{
        $source = imagecreatefromjpeg($_FILES['photo']['tmp_name']);
         $thumb = imagecreatetruecolor(1025, 365);
         //On refait l'image on lui donnant une taille constante
         imagecopyresized($thumb, $source, 0, 0, 0, 0, 1025,365, $size[0], $size[1]);
         imagejpeg($thumb,"admin/upload/".$_FILES['photo']['name']);
      }
       //move_uploaded_file($_FILES['photo']['tmp_name'],"admin/upload/".$_FILES['photo']['name']);
       echo '<p>La photo a bien été envoyée.</p>';
       echo '<img src="admin/upload/'.$_FILES['photo']['name'] . '">';
    }
    ?>
</center>