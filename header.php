<?php 
include('Connexion.php');
?>
<html>
  <head>
    <!-- Required meta tags -->
    <link rel="stylesheet" href="main.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
     <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Immersailles</title>
  </head>
  <body>
    <header class="w-100 text-light d-flex justify-content-between">
    <div class="h2 font-italic">
          <a href="index.php"><img src="images/logo.png " alt="logoImmersailles"></a>IMMERSAILLES
    </div>
    <div>
      <ul>
        <li class="liheader"><a class="text-white" href="EnSavoirPlus.php">A propos</a></li>
        <?php
          if(isset($_SESSION['role'])){
            if($_SESSION['role'] == 'Admin'){
              echo "<li class='liheader'><a class='text-white' href='deconnexion.php'>Déconnexion</a></li>";
            }
            if($_SESSION['role'] == 'Contributeur'){
              echo "<li class='liheader'><a class='text-white' href='deconnexion.php'>Déconnexion</a></li>";

            }
        }
        else{
          echo "<li class='liheader'><a class='text-white' href='Connexion2.php'>Connexion</a></li>";
        }
          ?> 
      </ul>
    </div>
    </header>
    <?php
    if(isset($_SESSION['login'])){
      if($_SESSION['role'] =="Admin"){
        include('navadmin.php');
      }
      else{
        include('navcontrib.php');
      }
    }
    ?>