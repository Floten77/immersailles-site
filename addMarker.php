<?php
if(!(isset($_SESSION['role']))){
	header('location:index.php');
}
include('header.php');
?>
<?php
//On verifie si le bouton ajouter a bien été selectionné
if (isset($_POST['ajouter'])){
	//On regarde si la longitude et la latitude n'est pas vide
	if($_POST['longitude']!="" && $_POST['latitude']!=""){
		$req = $dbh->prepare('INSERT INTO marqueur VALUES(?,?,?,?,?)');
		$sep = explode("-",$_POST['selectedlevel']);
		//On ajoute le marqueur dans la BDD
		$req->execute(array(NULL,$_POST['latitude'],$_POST['longitude'],$_POST['OHchoose'],$sep[1]));
	}
}
?>

<?php
	//On vérifie que l'utilisateur a selectionné un niveau avant d'afficher la carte
	if(isset($_POST['selectlevel']) || isset($_POST['ajouter'])){
		if($_POST['selectedlevel'] !=""){
			 $sep = explode("-",$_POST['selectedlevel']);
			 $url = $dbh->prepare('SELECT Cartes FROM niveaux,associer WHERE associer.IdLieu = niveaux.IdLieu AND IDannées = ? AND associer.IdLieu = ?  ');
		     $url->execute(array($sep[0],$sep[1]));
		    $res = $url->fetch();
		    $tabl = array();
		    if($res['Cartes'] !=""){
		        array_push($tabl,$res['Cartes']);
		    }
		    else{
		        array_push($tabl,"");
		    }
		}
	}
	else{
		$tabl = array();
		array_push($tabl,"");
	}
	//On recupère les marqueurs du niveau selectionnée
	$marqueur = array();
	$request = $dbh->prepare('SELECT * FROM marqueur WHERE IdLieu = ?');
	//On verifie que les niveaux sont selectionnées
	if(isset($_POST['selectlevel'])|| isset($_POST['ajouter'])){
		$request->execute(array($sep[1]));
		while($donnees = $request ->fetch()){
		$position = array();
		array_push($position,intval($donnees['IDmarqueur']));
		array_push($position,doubleval($donnees['x']));
		array_push($position,doubleval($donnees['y']));
		array_push($marqueur,$position);
	}
	}

?>
<center><p class="h2" style="height:3%">Selectionner un niveau</p></center>
<?php
 	//Affichade des niveaux 
	if(isset($_POST["selectlevel"]) || isset($_POST['ajouter'])){
		if($_POST['selectedlevel'] != ""){
			if($sep[1]==101)
			echo "<p style='margin-left:42%'>Niveau selectionnée : Année:".$sep[0]."-RDC</p>";
			if($sep[1]==102)
				echo "<p style='margin-left:42%'>Niveau selectionnée : Année:".$sep[0]."-1ere étage</p>";
			if($sep[1]==103)
				echo "<p style='margin-left:42%'>Niveau selectionnée : Année:".$sep[0]."-2eme étage</p>";
		}
	}
?>
<form style="margin-left:45%" method="POST">
	<select name="selectedlevel" >
		<?php
		//Affichage des niveaux associer
		$req = 'SELECT IDannées,associer.IdLieu,nomlieu FROM associer,niveaux WHERE associer.IdLieu = niveaux.IdLieu';
		$request = $dbh->query($req);
		while($donnees = $request->fetch()){
			echo "<option value =".$donnees['IDannées']."-".$donnees['IdLieu'].">".$donnees['IDannées']."-".$donnees['nomlieu']."</option>";
		}
		?>
	</select>
	<input type="submit" value="Valider" name="selectlevel">
</form>
<?php
	if(isset($_POST['selectlevel']) || isset($_POST['ajouter'])){
		echo'<div id="map" style="width:75%;margin-left:12%;height: 50%;"></div>';
	}
?>
<?php
 if(isset($_POST['selectlevel']) || isset($_POST['ajouter'])){ ?>
<form style="margin-left:28%"action="" method ="POST">
	<label>Latitude:</label>
	<input type="text" id="latitude" name="latitude"  readonly="readonly" value="">
	<label>Longitude:</label>
	<input type="text" id ="longitude" name="longitude" readonly="readonly" value ="" >
	<label>Choisir un objet historique:</label>
	<select name="OHchoose">
	<?php 
	//Afficher tous les objets historiques dans le select
		$request = $dbh->prepare('SELECT * FROM objethistoriques');
		$request->execute();
		while($results = $request ->fetch()){
			echo "<option value='".$results['IDObject']."'>".$results['NomObjet']."</option>";
		}
	?>
	</select>
	<?php
	if(isset($_POST['selectedlevel'])){
		echo "<input type='hidden' name='selectedlevel' value='".$_POST['selectedlevel']."'/>";
	}
	?>
	<input type="submit" name="ajouter" value="Ajouter le marqueur">
	<input type="reset" value="Reset">
</form>
<?php } ?>
<?php
if(isset($_POST['ajouter'])){
	//Affichage du résultats de l'ajout des marqueurs
	if($_POST['longitude']!="" && $_POST['latitude']!=""){
			echo '<p style="margin-left:37%">Le marqueur à la position :('.$_POST['longitude'].','.$_POST['latitude'].') a bien été ajouté.</p>';
		}
		else{
			echo'<p style="margin-left:40%">Des valeurs sont manquants pour pouvoir ajouter un marqueur</p>';
		}
}
?>


<script>
		//
		if(<?php echo json_encode($tabl[0]); ?> !=""){
             var url = "reconnaissance/affectation/"+<?php echo json_encode($tabl[0]); ?>;
        }
        else{
            var url="images/reconstitue.png";
        }
		var map = L.map('map', {
		crs: L.CRS.Simple,
		minZoom: -1,maxZoom:10
		});
		var bounds = [[-26.5,-25], [1021.5,1023]];
		var image = L.imageOverlay(url, bounds).addTo(map);
		map.fitBounds(bounds);
		map.setView( [70, 120], 1);

		var popup = L.popup();
		//Renvoie les coordonnées du marqueur lorsqu'on clique sur la map
		map.on('click', function(e){
		 popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString())
        .openOn(map);
        //Donne la valeur des coordonnées du curseur dans les input
        $('#latitude').val(e.latlng.lat);
        $('#longitude').val(e.latlng.lng);
	});
		var markers = []
function createMarker(coords,ids) {
  var id
  id =ids
  var popupContent =
    '<p>Marker selected</p></br>'
  myMarker = L.marker(coords, {
    draggable: false
  });
  myMarker._id = id
  var myPopup = myMarker.bindPopup(popupContent, {
    closeButton: false
  });
  map.addLayer(myMarker)
  markers.push(myMarker)
}
//Genère les marqueurs dans la map
var coordinates = <?php echo json_encode($marqueur);?>;
for (i = 0;i<coordinates.length;i++){
	var coord = []
	coord.push(coordinates[i][1]);
	coord.push(coordinates[i][2]);
	console.log(coordinates[i]);
	createMarker(coord,coordinates[i][0]);
    	}

</script>